﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using AutoMapper;
using JondellCorp.API.Controllers;
using JondellCorp.API.Data;
using JondellCorp.API.Dtos;
using JondellCorp.API.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace JondellCorp.API.Tests.Controllers
{
    [TestClass]
    public class BalanceControllerTest
    {
        [TestMethod]
        public async Task SaveBalanceReturnsOk()
        {
            // Arrange
            Mapper.Initialize(config => {
                config.CreateMap<BalanceToCreateDto, Balance>();
                config.CreateMap<Balance, BalanceToCreateDto>();
            });

            var balanceDto = new BalanceToCreateDto
            {
                Year = 2019,
                Month = 1,
                Canteen = 100,
                CEOsCar = 150,
                Marketing = 200,
                ParkingFines = 250,
                RandD = 300
            };

            var balanceToCreate = Mapper.Map<BalanceToCreateDto, Balance>(balanceDto);

            var mockRepository = new Mock<IBalanceRepository> {  CallBase = true };
            mockRepository.Setup(x => x.SaveBalance(balanceToCreate)).ReturnsAsync(new Balance
            {
                Year = 2019,
                Month = 1,
                Canteen = 100,
                CEOsCar = 150,
                Marketing = 200,
                ParkingFines = 250,
                RandD = 300
            });
            var controller = new BalanceController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = await controller.Save(balanceDto);
            var contentResult = actionResult as OkNegotiatedContentResult<BalanceToReturnDto>;

            // Assert
            Assert.IsNotNull(contentResult);
        }

        [TestMethod]
        public async Task GetBalanceReturnsOk()
        {
            // Arrange
            var balanceReq = new BalanceRequestDto
            {
                Year = 2019,
                Month = 1
            };

            var mockRepository = new Mock<IBalanceRepository> { CallBase = true };
            mockRepository.Setup(x => x.GetBalance(balanceReq.Year, balanceReq.Month)).ReturnsAsync(new Balance
            {
                Year = 2019,
                Month = 1,
                Canteen = 100,
                CEOsCar = 150,
                Marketing = 200,
                ParkingFines = 250,
                RandD = 300
            });
            var controller = new BalanceController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = await controller.GetBalance(balanceReq);
            var contentResult = actionResult as OkNegotiatedContentResult<Balance>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(balanceReq.Year, contentResult.Content.Year);
            Assert.AreEqual(balanceReq.Month, contentResult.Content.Month);
        }

        [TestMethod]
        public async Task YearListReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<IBalanceRepository> { CallBase = true };
            mockRepository.Setup(x => x.GetYearList()).ReturnsAsync(new List<Int32>
            {
                2017, 2018, 2019
            });
            var controller = new BalanceController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = await controller.YearList();
            var contentResult = actionResult as OkNegotiatedContentResult<List<Int32>>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
        }
    }
}
