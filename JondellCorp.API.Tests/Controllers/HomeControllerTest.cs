﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JondellCorp.API;
using JondellCorp.API.Controllers;

namespace JondellCorp.API.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Jondell Corp", result.ViewBag.Title);
        }
    }
}
