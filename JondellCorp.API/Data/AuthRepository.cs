﻿using JondellCorp.API.Dtos;
using JondellCorp.API.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Security;

namespace JondellCorp.API.Data
{
    public class AuthRepository : IAuthRepository
    {
        private DatabaseContext _context;
        private UserManager<IdentityUser> _userManager;

        public AuthRepository()
        {
            _context = new DatabaseContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_context));
        }

        public async Task<IdentityResult> RegisterUser(UserForRegisterDto userModel)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = userModel.Username
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);
            if (result.Succeeded)
            {
                var roleStore = new RoleStore<IdentityRole>(_context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                await roleManager.CreateAsync(new IdentityRole { Name = userModel.Role });
                await _userManager.AddToRoleAsync(user.Id, userModel.Role);  
            }
            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);
            return user;
        }
        public async Task<ClaimsIdentity> GetIdentity(OAuthGrantResourceOwnerCredentialsContext context, IdentityUser user)
        {
            return await _userManager.CreateIdentityAsync(user,
                context.Options.AuthenticationType);
        }
        public async Task<List<string>> GetUserRoles(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);
            if (user != null)
            {
                List<string> roleList = new List<string>();

                var roleStore = new RoleStore<IdentityRole>(_context);
                var roleMngr = new RoleManager<IdentityRole>(roleStore);

                var rolesInDb = roleMngr.Roles.ToArray();
                foreach (var role in rolesInDb)
                {
                    var userRole = new IdentityUserRole
                    {
                        RoleId = role.Id,
                        UserId = user.Id
                    };
                    if (user.Roles.ToList().Find(x => x.RoleId == userRole.RoleId) != null)
                        roleList.Add(role.Name);
                }
                return roleList;
            }
            else return null;
        }
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                    _userManager.Dispose();
                    _context = null;
                    _userManager = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}