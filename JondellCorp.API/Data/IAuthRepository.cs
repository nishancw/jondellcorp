﻿using JondellCorp.API.Dtos;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace JondellCorp.API.Data
{
    public interface IAuthRepository : IDisposable
    {
        Task<IdentityResult> RegisterUser(UserForRegisterDto userModel);
        Task<IdentityUser> FindUser(string userName, string password);
        Task<ClaimsIdentity> GetIdentity(OAuthGrantResourceOwnerCredentialsContext context, IdentityUser user);
        Task<List<string>> GetUserRoles(string userName, string password);
    }
}
