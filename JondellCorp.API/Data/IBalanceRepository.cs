﻿using JondellCorp.API.Dtos;
using JondellCorp.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JondellCorp.API.Data
{
    public interface IBalanceRepository : IDisposable
    {
        Task<BalanceToReturnDto> GetLastBalance();
        Task<Balance> GetBalance(int year, int month);
        Task<Balance> SaveBalance(Balance balance);
        Task<List<ReportForReturnDto>> GetReport(ReportRequestDto report);
        Task<List<Int32>> GetYearList();
    }
}
