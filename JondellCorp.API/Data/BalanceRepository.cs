﻿using AutoMapper;
using JondellCorp.API.Dtos;
using JondellCorp.API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace JondellCorp.API.Data
{
    public class BalanceRepository : IBalanceRepository
    {
        private DatabaseContext _context;

        public BalanceRepository()
        {
            _context = new DatabaseContext();
        }

        public async Task<BalanceToReturnDto> GetLastBalance()
        {
            var lastBalance = await Task.FromResult(_context.Balances.OrderByDescending(b => b.Year).ThenByDescending(b => b.Month).First());
            if (lastBalance != null)
                return Mapper.Map<Balance, BalanceToReturnDto>(lastBalance);
            return null;
        }
        public async Task<Balance> GetBalance(int year, int month)
        {
            return await _context.Balances.FindAsync(year, month);
        }

        public async Task<Balance> SaveBalance(Balance balance)
        {
            _context.Set<Balance>().AddOrUpdate(balance);
            await _context.SaveChangesAsync();
            return await GetBalance(balance.Year, balance.Month);
        }

        public async Task<List<ReportForReturnDto>> GetReport(ReportRequestDto report)
        {
            IQueryable<Balance> reportToReturn = _context.Balances.OrderBy(b => b.Year).ThenBy(b => b.Month).AsQueryable();
            if(report.StartYear < report.EndYear)
            {
                reportToReturn = reportToReturn.Where(x =>
                    x.Year == report.StartYear && x.Month >= report.StartMonth 
                    || x.Year > report.StartYear && x.Year < report.EndYear 
                    || x.Year == report.EndYear && x.Month <= report.EndMonth);
            }

            if (report.StartYear == report.EndYear)
            {
                reportToReturn = reportToReturn.Where(x => 
                    x.Month >= report.StartMonth && x.Month <= report.EndMonth && x.Year == report.StartYear);
            }

            List<ReportForReturnDto> result = null;
            switch (report.Account)
            {
                case "randd":
                    result = await reportToReturn.Select(r => new ReportForReturnDto
                    {
                        Year = r.Year,
                        Month = r.Month,
                        AccBalance = r.RandD
                    }).ToListAsync();
                    break;
                case "canteen":
                    result = await reportToReturn.Select(r => new ReportForReturnDto
                    {
                        Year = r.Year,
                        Month = r.Month,
                        AccBalance = r.Canteen
                    }).ToListAsync();
                    break;
                case "ceoscar":
                    result = await reportToReturn.Select(r => new ReportForReturnDto
                    {
                        Year = r.Year,
                        Month = r.Month,
                        AccBalance = r.CEOsCar
                    }).ToListAsync();
                    break;
                case "marketing":
                    result = await reportToReturn.Select(r => new ReportForReturnDto
                    {
                        Year = r.Year,
                        Month = r.Month,
                        AccBalance = r.Marketing
                    }).ToListAsync();
                    break;
                case "parkingFines":
                    result = await reportToReturn.Select(r => new ReportForReturnDto
                    {
                        Year = r.Year,
                        Month = r.Month,
                        AccBalance = r.ParkingFines
                    }).ToListAsync();
                    break;
                default:
                    result = null;
                    break;
            }
            return result;
        }

        public async Task<List<Int32>> GetYearList()
        {
            List<Int32> years = new List<Int32>();
            List<Balance> result = await _context.Balances.GroupBy(x => x.Year).Select(x => x.FirstOrDefault()).ToListAsync();
            foreach (var item in result)
            {
                years.Add(item.Year);
            }
            return years;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                    _context = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}