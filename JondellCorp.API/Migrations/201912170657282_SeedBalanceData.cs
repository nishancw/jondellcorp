namespace JondellCorp.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedBalanceData : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO [dbo].[Balances] ([Year], [Month], [RandD], [Canteen], [CEOsCar], [Marketing], [ParkingFines]) VALUES (2018, 10, 440, 52000, -12000, 45500.6, 440.4)
                INSERT INTO [dbo].[Balances] ([Year], [Month], [RandD], [Canteen], [CEOsCar], [Marketing], [ParkingFines]) VALUES (2018, 11, 440, 52000, -12000, 45500.6, 440.9)
                INSERT INTO [dbo].[Balances] ([Year], [Month], [RandD], [Canteen], [CEOsCar], [Marketing], [ParkingFines]) VALUES (2018, 12, 0, 11, -12000, 45500.6, 440.9)
                INSERT INTO [dbo].[Balances] ([Year], [Month], [RandD], [Canteen], [CEOsCar], [Marketing], [ParkingFines]) VALUES (2019, 1, 10500.5, 4200, 10000, 1500, -32000)
                INSERT INTO [dbo].[Balances] ([Year], [Month], [RandD], [Canteen], [CEOsCar], [Marketing], [ParkingFines]) VALUES (2019, 2, 5000, 1200, 1200, 1200, -32000)
                INSERT INTO [dbo].[Balances] ([Year], [Month], [RandD], [Canteen], [CEOsCar], [Marketing], [ParkingFines]) VALUES (2019, 3, 45200, 15000, 900, 1566.5, 12.55)
                INSERT INTO [dbo].[Balances] ([Year], [Month], [RandD], [Canteen], [CEOsCar], [Marketing], [ParkingFines]) VALUES (2019, 4, 63000, 12000, 4566, 1522.5, 12600)
                INSERT INTO [dbo].[Balances] ([Year], [Month], [RandD], [Canteen], [CEOsCar], [Marketing], [ParkingFines]) VALUES (2019, 5, 45000, 4500.6, 12006.5, 12244.5, 12200)
                INSERT INTO [dbo].[Balances] ([Year], [Month], [RandD], [Canteen], [CEOsCar], [Marketing], [ParkingFines]) VALUES (2019, 6, 120000, 451200, 12200, -4500, 36000)
                INSERT INTO [dbo].[Balances] ([Year], [Month], [RandD], [Canteen], [CEOsCar], [Marketing], [ParkingFines]) VALUES (2019, 7, 10500.5, 4200, 10000, 1500, -32000)
            ");
        }
        
        public override void Down()
        {
        }
    }
}
