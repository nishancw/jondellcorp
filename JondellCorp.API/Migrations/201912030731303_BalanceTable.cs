namespace JondellCorp.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BalanceTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Balances",
                c => new
                    {
                        Year = c.Int(nullable: false),
                        Month = c.Short(nullable: false),
                        RandD = c.Double(nullable: false),
                        Canteen = c.Double(nullable: false),
                        CEOsCar = c.Double(nullable: false),
                        Marketing = c.Double(nullable: false),
                        ParkingFines = c.Double(nullable: false),
                    })
                .PrimaryKey(t => new { t.Year, t.Month });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Balances");
        }
    }
}
