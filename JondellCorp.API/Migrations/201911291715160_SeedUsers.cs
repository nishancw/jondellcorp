namespace JondellCorp.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'59693484-a999-4f35-83de-be4bb6ce3b38', NULL, 0, N'ABd9H/lKrYkD9MJO5FzJ0ceCMxq6wzoNrNHRdpekrgWI6ME+mCnQyVap/GherhnvaA==', N'82e105a6-3cb2-4a19-8e6f-133366941046', NULL, 0, 0, NULL, 0, 0, N'Admin')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b2968337-e2ea-4e16-a684-943c14d093bc', NULL, 0, N'AIwWiVOic+5NljeL0VQgJVUAOqhtdNF2P5FF2V7/s+AyM06PQx8MTi59giUDToORkg==', N'11fd1116-996f-46cd-a137-e45585e8445b', NULL, 0, 0, NULL, 0, 0, N'User')
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'39b0a061-c2dd-49c8-bc5a-8290a8d8fc24', N'CanUploadAndViewReport')
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'dda73520-5b6f-4258-a33d-fe244a6e129b', N'CanViewBalance')
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'59693484-a999-4f35-83de-be4bb6ce3b38', N'39b0a061-c2dd-49c8-bc5a-8290a8d8fc24')
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b2968337-e2ea-4e16-a684-943c14d093bc', N'dda73520-5b6f-4258-a33d-fe244a6e129b')
                ");
        }
        
        public override void Down()
        {
        }
    }
}
