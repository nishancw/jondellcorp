﻿using AutoMapper;
using JondellCorp.API.Data;
using JondellCorp.API.Dtos;
using JondellCorp.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace JondellCorp.API.Controllers
{
    [RoutePrefix("api/Balance")]
    public class BalanceController : ApiController
    {
        private IBalanceRepository _repo;
        public BalanceController(IBalanceRepository repo)
        {
            _repo = repo;
        }

        [Authorize(Roles = "CanUploadAndViewReport")]
        [HttpPost]
        [Route("save")]
        public async Task<IHttpActionResult> Save(BalanceToCreateDto balance)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var balanceToCreate = Mapper.Map<BalanceToCreateDto, Balance>(balance);
            Balance result = await _repo.SaveBalance(balanceToCreate);

            var balanceToReturn = Mapper.Map<Balance, BalanceToReturnDto>(result);
            return Ok(balanceToReturn);
        }

        [Authorize(Roles = "CanUploadAndViewReport")]
        [HttpGet]
        [Route("report")]
        public async Task<IHttpActionResult> Report([FromUri] ReportRequestDto report)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(report.StartYear > report.EndYear || (report.StartYear == report.EndYear && report.StartMonth > report.EndMonth))
            {
                return BadRequest("Invalid request");
            }

            var result = await _repo.GetReport(report);
            if(result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Authorize(Roles = "CanViewBalance, CanUploadAndViewReport")]
        [HttpGet]
        [Route("yearlist")]
        public async Task<IHttpActionResult> YearList()
        {
            var result = await _repo.GetYearList();
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Authorize(Roles = "CanViewBalance, CanUploadAndViewReport")]
        [HttpGet]
        [Route("last")]
        public async Task<IHttpActionResult> GetLastBalance()
        {
            var result = await _repo.GetLastBalance();
            if (result == null)
                return NotFound();
            return Ok(result);
        }

        [Authorize(Roles = "CanViewBalance, CanUploadAndViewReport")]
        [HttpGet]
        [Route("balance")]
        public async Task<IHttpActionResult> GetBalance([FromUri] BalanceRequestDto request)
        {
            var result = await _repo.GetBalance(request.Year, request.Month);
            if (result == null)
                return NotFound();
            return Ok(result);
        }

    }
}
