﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JondellCorp.API.Models
{
    public class Balance
    {
        [Key, Column(Order = 1)]
        public Int32 Year { get; set; }
        [Key, Column(Order = 2)]
        public Int16 Month { get; set; }
        public double RandD { get; set; }
        public double Canteen { get; set; }
        public double CEOsCar { get; set; }
        public double Marketing { get; set; }
        public double ParkingFines { get; set; }
    }
}