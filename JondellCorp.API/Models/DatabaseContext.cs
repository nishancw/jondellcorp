﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JondellCorp.API.Models
{
    public class DatabaseContext : IdentityDbContext<IdentityUser>
    {
        public DatabaseContext()
            : base("JondellCorpConnectionString")
        {

        }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }

        public DbSet<Balance> Balances { get; set; }
    }
}