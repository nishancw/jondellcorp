﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JondellCorp.API.Dtos
{
    public class ReportForReturnDto
    {
        public Int32 Year { set; get; }
        public Int16 Month { set; get; }
        public double AccBalance { get; set; }
    }
}