﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JondellCorp.API.Dtos
{
    public class BalanceToReturnDto
    {
        public Int32 Year { get; set; }
        public Int16 Month { get; set; }
        public double RandD { get; set; }
        public double Canteen { get; set; }
        public double CEOsCar { get; set; }
        public double Marketing { get; set; }
        public double ParkingFines { get; set; }
    }
}