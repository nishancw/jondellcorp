﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JondellCorp.API.Dtos
{
    public class ReportRequestDto
    {
        [Required]
        public Int32 StartYear { get; set; }
        [Required]
        public Int16 StartMonth { get; set; }
        [Required]
        public Int32 EndYear { get; set; }
        [Required]
        public Int16 EndMonth { get; set; }
        [Required]
        public string Account { get; set; }
    }
}