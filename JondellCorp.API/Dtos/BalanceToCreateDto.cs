﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JondellCorp.API.Dtos
{
    public class BalanceToCreateDto
    {
        [Required]
        public Int32 Year { get; set; }
        [Required]
        public Int16 Month { get; set; }
        [Required]
        public double RandD { get; set; }
        [Required]
        public double Canteen { get; set; }
        [Required]
        public double CEOsCar { get; set; }
        [Required]
        public double Marketing { get; set; }
        [Required]
        public double ParkingFines { get; set; }
    }
}