﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JondellCorp.API.Dtos
{
    public class BalanceRequestDto
    {
        [Required]
        public Int32 Year { get; set; }
        [Required]
        public Int16 Month { get; set; }
    }
}