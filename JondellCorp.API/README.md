# Jondell Corp API
**Login**
----
  Returns json responce with token, user, role.

* **URL**

  [host]/login

* **Method:**

  `POST`
  
*  **URL Params**
    	'username' : 'usernameString'
    	'password' : 'passwordString'
    	'grant_type' : password

* **Headers**

	` 'Content-type': 'application/x-www-form-urlencoded'`

* **Data Params**

	`null`


* **Success Response:**

  - **Code:** 200
  - **Content:** 
        { 
		"access_token": "4ZW......WBM-f0",
        "token_type": "bearer",
        "expires_in": 3599,
        "userName": "username",
        "roles": "[\"CanUploadAndViewReport\"]",
        ".issued": "Sat, 14 Dec 2019 14:25:02 GMT",
        ".expires": "Sat, 14 Dec 2019 15:25:02 GMT"
		}
 
* **Error Response:**

  - **Code:** 400
  - **Content:** 
    	{
        "error": "invalid_grant",
        "error_description": "The user name or password is incorrect."
    	}

  OR

  - **Code:** 400
  - **Content:** 
    	{
        "error": "unsupported_grant_type"
    	}

**Register**
----
  Returns json data about a single user.

* **URL**

  [host]/api/account/register

* **Method:**

  `POST`
  
*  **URL Params**
    
    `null`

* **Headers**

	` 'Content-type': 'application/json'`

* **Data Params**
  {
  "username": "username",
  "password": "password",
  "role": "CanUploadAndViewReport"
  }


* **Success Response:**

  - **Code:** 201
  - **Content:** 
        { }
 
* **Error Response:**

  - **Code:** 400 
  - **Content:** 
{
    "message": "The request is invalid.",
    "modelState": {
        "": [
            "Name User is already taken."
        ]
    }
}

  OR

  - **Code:** 400
  - **Content:** 
{
    "message": "The request is invalid.",
    "modelState": {
        "userModel.Username": [
            "The Username field is required."
        ]
    }
}

**Save Balance**
----
  Returns json data about a single user.

* **URL**

  [host]/api/balance/save

* **Method:**

  `POST`
  
*  **URL Params**
    
    `null`

* **Headers**

	` 'Content-type': 'application/json'`
    `Authorization : Bearer 7mHO2Qd...`

* **Data Params**
{
    "year": 2018,
    "month": 12,
    "randd": 4400,
    "canteen": 52000,
    "ceoscar": -12000,
    "marketing": 45500.6,
    "parkingFines": 4200.9
}


* **Success Response:**

  - **Code:** 200
  - **Content:** 
{
    "year": 2018,
    "month": 12,
    "randD": 4400,
    "canteen": 52000,
    "ceOsCar": -12000,
    "marketing": 45500.6,
    "parkingFines": 4200.9
}
 
* **Error Response:**

  - **Code:** 401 
  - **Content:** 
{
    "message": "Authorization has been denied for this request."
}

**Get Report Data By Start/End**
----
  Returns json data about a single user.

* **URL**

  [host]/api/balance/report

* **Method:**

  `GET`
  
*  **URL Params**
    
    `?startyear=2019&endyear=2019&startmonth=1&endmonth=3&account=canteen`

* **Headers**

	` 'Content-type': 'application/json'`
    `Authorization : Bearer 7mHO2Qd...`

* **Data Params**
  `NULL`


* **Success Response:**

  - **Code:** 200
  - **Content:** 
[
    {
        "year": 2019,
        "month": 1,
        "accBalance": 4200
    },
    {
        "year": 2019,
        "month": 2,
        "accBalance": 1200
    },
    {
        "year": 2019,
        "month": 3,
        "accBalance": 15000
    }
]
 
* **Error Response:**

  - **Code:** 400 
  - **Content:** 
{
    "message": "Invalid request"
}

  OR

  - **Code:** 401
  - **Content:** 
{
    "message": "Authorization has been denied for this request."
}

**Get Report Data/ Balance By Month**
----
  Returns json data about a single user.

* **URL**

  [host]/api/balance/balance

* **Method:**

  `GET`
  
*  **URL Params**
    
    `?year=2019&month=1`

* **Headers**

	` 'Content-type': 'application/json'`
    `Authorization : Bearer 7mHO2Qd...`

* **Data Params**
  `NULL`


* **Success Response:**

  - **Code:** 200
  - **Content:** 
{
    "year": 2019,
    "month": 1,
    "randD": 10500.5,
    "canteen": 4200,
    "ceOsCar": 10000,
    "marketing": 1500,
    "parkingFines": -32000
}
 
* **Error Response:**

  - **Code:** 404
  - **Content:** 
`NULL`

  OR

  - **Code:** 401
  - **Content:** 
{
    "message": "Authorization has been denied for this request."
}

**Return List of Available Years**
----
  Returns json data about a single user.

* **URL**

  [host]/api/balance/yearlist

* **Method:**

  `GET`
  
*  **URL Params**
    
    `NULL`

* **Headers**

	` 'Content-type': 'application/json'`
    `Authorization : Bearer 7mHO2Qd...`

* **Data Params**
  `NULL`


* **Success Response:**

  - **Code:** 200
  - **Content:** 
[
    2018,
    2019
]
 
* **Error Response:**

  - **Code:** 404
  - **Content:** 
`NULL`

  OR

  - **Code:** 401
  - **Content:** 
{
    "message": "Authorization has been denied for this request."
}

**Return Last Available Balance**
----
  Returns json data about a single user.

* **URL**

  [host]/api/balance/last

* **Method:**

  `GET`
  
*  **URL Params**
    
    `NULL`

* **Headers**

	` 'Content-type': 'application/json'`
    `Authorization : Bearer 7mHO2Qd...`

* **Data Params**
  `NULL`


* **Success Response:**

  - **Code:** 200
  - **Content:** 
{
    "year": 2019,
    "month": 8,
    "randD": 10500.5,
    "canteen": 4200,
    "ceOsCar": 10000,
    "marketing": 200000,
    "parkingFines": -32000
}
 
* **Error Response:**

  - **Code:** 404
  - **Content:** 
`NULL`

  OR

  - **Code:** 401
  - **Content:** 
{
    "message": "Authorization has been denied for this request."
}