﻿using AutoMapper;
using JondellCorp.API.Dtos;
using JondellCorp.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JondellCorp.API.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Balance, BalanceToReturnDto>();
            Mapper.CreateMap<Balance, BalanceToCreateDto>();
            Mapper.CreateMap<BalanceToReturnDto, Balance>();
            Mapper.CreateMap<BalanceToCreateDto, Balance>();
        }
    }
}