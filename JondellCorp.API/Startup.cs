﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using JondellCorp.API.Data;
using JondellCorp.API.Helpers;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(JondellCorp.API.Startup))]

namespace JondellCorp.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/login"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(6),
                Provider = new AuthProvider()
            };
            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            HttpConfiguration config = new HttpConfiguration();
            UnityConfig.RegisterComponents(config);
            WebApiConfig.Register(config);
            app.UseWebApi(config);
        }
    }
}
