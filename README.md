# ADRA Developer Test


**Assumptions**

- Number of accounts and names of the accounts do not change overtime.

- The account balance excel file has the balances on 1st sheet and has the following [format](./sample.xlsx) and does not have year info, as the test suggested.

    ![](./sample.jpg)