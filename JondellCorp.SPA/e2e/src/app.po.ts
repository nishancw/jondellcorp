import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-home h1')).getText() as Promise<string>;
  }

  getLoginButtonText() {
    return element(by.css('form button')).getText() as Promise<string>;
  }

  getWelcomeText() {
    return element(by.xpath('//*[@id="navbar"]/ul[2]/li/a')).getText() as Promise<string>;
  }
}
