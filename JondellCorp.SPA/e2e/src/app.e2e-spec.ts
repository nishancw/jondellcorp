import { AppPage } from './app.po';
import { browser, logging, by, element } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display jondell corp', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Jondell Corp');
  });

  it('should display login form', () => {
    page.navigateTo();
    expect(page.getLoginButtonText()).toEqual('Sign in');
  });

  it('should be able to login', () => {
    browser.ignoreSynchronization = true;
    const user = browser.driver.findElement(by.name('username'));
    const password = browser.driver.findElement(by.name('password'));
    const button = element(by.css('form button'));

    user.sendKeys('User');
    password.sendKeys('password');

    expect(user.getAttribute('value')).toEqual('User');
    expect(password.getAttribute('value')).toEqual('password');

    button.click().then(() => {
        browser.waitForAngular();
        browser.driver.sleep(3000);
        expect(page.getWelcomeText()).toEqual('Welcome User');
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
