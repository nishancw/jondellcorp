import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import { BalanceToCreate } from '../_models/BalanceToCreate';
import { BalanceService } from '../_services/balance.service';
import { AlertifyService } from '../_services/alertify.service';
import { Balance } from '../_models/Balance';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  model: any = {};
  arrayBuffer: any;
  file: File;
  emptyHead = '__EMPTY';
  balance: BalanceToCreate;
  year = new Date().getFullYear();
  month = 0;
  header = '';
  accounts: number[] = [5];
  fileAvailable = false;
  fileTypes = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel'];
  validFile = false;
  accountNamesOnExcel = [ 'R&D', 'Canteen', 'CEO’s car', 'Marketing', 'Parking fines' ];
  months = [ 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december' ];
  constructor(private balanceService: BalanceService, private alertify: AlertifyService) { }

  ngOnInit() {
  }

  incomingfile(event) {
    this.validFile = false;
    this.file = event.target.files[0];
    if (!(this.fileTypes.includes(this.file.type))) {
      this.alertify.error('Please select either .xls or .xlsx file');
    } else {
      this.validFile = true;
    }
  }

  Process() {
    const reg = new RegExp('/^-?\d+\.?\d*$/');
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, {type: 'binary'});
      const firstSheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[firstSheetName];
      const xcelObject = (XLSX.utils.sheet_to_json(worksheet, {raw: true}));

      const cellref = XLSX.utils.encode_cell({c: 0 , r: 0}); // construct A1 reference for cell
      const cell = worksheet[cellref];
      this.header = cell.v;
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < xcelObject.length; i++) {
        this.accounts[i] = Number(xcelObject[i][this.emptyHead]) ? xcelObject[i][this.emptyHead] : 0;
        if (this.accounts[i] === 0) {
          this.alertify.message('Value at ' + xcelObject[i][this.header] + 'interprited as 0, ignore if intended');
        }
        if (xcelObject[i][this.header].toLowerCase().replace(/\s/g, '') !== this.accountNamesOnExcel[i].toLowerCase().replace(/\s/g, '')) {
          this.alertify.error(this.accountNamesOnExcel[i].toLowerCase() + ' shoud appear as no. ' + (i + 1) + ' entry in the file.' );
          this.fileAvailable = false;
        }
      }
      this.month = this.months.indexOf(cell.v.split(' ').splice(-1)[0].toLowerCase()) + 1;
    };
    fileReader.readAsArrayBuffer(this.file);
    this.fileAvailable = true;
  }

  cancel() {
    this.fileAvailable = false;
  }

  sendData() {
    this.model.year = this.year;
    this.model.month = this.month;
    this.model.randd = this.accounts[0];
    this.model.canteen = this.accounts[1];
    this.model.ceoscar = this.accounts[2];
    this.model.marketing = this.accounts[3];
    this.model.parkingFines = this.accounts[4];
    // console.log(this.model);
    this.balanceService.sendBalance(this.model).subscribe((responce) => {
      this.alertify.success('Balance saved');
    }, error => {
      this.alertify.error(error ? 'Server error occoured' : error);
    });
  }
}
