import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Balance } from '../_models/Balance';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BalanceToCreate } from '../_models/BalanceToCreate';
import { Report } from '../_models/Report';

@Injectable({
  providedIn: 'root'
})
export class BalanceService {
  baseUrl = environment.apiUrl;
  constructor(private http: HttpClient, private router: Router) { }

  getLastBalance(): Observable<Balance> {
    const token = localStorage.getItem('token');
    let httpOptions = null;
    if (token) {
      httpOptions = {
        headers: new HttpHeaders({
          Authorization:  'Bearer ' + token
        })
      };
      // const headers = new HttpHeaders({Authorization: 'Bearer ' + token});
      // options = { headers };
    }
    return this.http.get(this.baseUrl + 'api/balance/last', httpOptions)
    .pipe(map(response => response as unknown as Balance))
    .pipe(
      catchError(this.handleError)
    );
  }

  getBalance(userParams: any): Observable<Balance> {
    // console.log('userParams',userParams);
    const token = localStorage.getItem('token');
    let options = null;
    let queryString = '?';
    if (token) {
      const headers = new HttpHeaders({Authorization: 'Bearer ' + token});
      options = { headers };
    }
    if (userParams != null) {
      queryString +=
        'year=' + userParams.year +
        '&month=' + userParams.month;
    }
    // console.log('queryString',queryString);
    return this.http.get(this.baseUrl + 'api/balance/balance' + queryString, options)
    .pipe(map(response => response as unknown as Balance))
    .pipe(
      catchError(this.handleError)
    );
  }

  sendBalance(model: BalanceToCreate) {
    const token = localStorage.getItem('token');
    let options = null;
    if (token) {
      const headers = new HttpHeaders({Authorization: 'Bearer ' + token, 'Content-type': 'application/json'});
      options = { headers };
    }
    return this.http.post<BalanceToCreate>(this.baseUrl + 'api/balance/save', model, options)
    .pipe(
      catchError(this.handleError)
    );
  }

  getReport(userParams: any): Observable<Report[]> {
    const token = localStorage.getItem('token');
    let options = null;
    let queryString = '?';
    if (token) {
      const headers = new HttpHeaders({Authorization: 'Bearer ' + token});
      options = { headers };
    }
    if (userParams != null) {
      queryString +=
        'startyear=' + userParams.startYear +
        '&endyear=' + userParams.endYear +
        '&startmonth=' + userParams.startMonth +
        '&endmonth=' + userParams.endMonth +
        '&account=' + userParams.account;
    }
    return this.http.get(this.baseUrl + 'api/balance/report' + queryString, options)
    .pipe(map(response => response as unknown as Report[] ))
    .pipe(
      catchError(this.handleError)
    );
  }

  getYearList() {
    const token = localStorage.getItem('token');
    let options = null;
    if (token) {
      const headers = new HttpHeaders({Authorization: 'Bearer ' + token});
      options = { headers };
    }
    return this.http.get(this.baseUrl + 'api/balance/yearlist', options)
    .pipe(map(response => response))
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: any) {
    // console.log(error);
    if (error.status === 404) {
      return throwError('There\'s no data yet!');
    }
    if (error.status === 401) {
      // this.router.navigate(['/home']);
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      localStorage.removeItem('isAdmin');
      return throwError('Please login again!');
    }
    const applicationError = error.error.message;
    if (applicationError) {
      // this.router.navigate(['/home']);
      // localStorage.removeItem('token');
      // localStorage.removeItem('user');
      // localStorage.removeItem('isAdmin');
      return throwError(applicationError + ', check for possible mistakes');
    }
    const serverError = error.error.modelState;
    let modelStateErrors = '';
    if (serverError) {
      for (const key in serverError) {
        if (serverError[key]) {
          modelStateErrors += serverError[key] + '\n';
        }
      }
    }
    return throwError(modelStateErrors || 'Server error');
  }
}
