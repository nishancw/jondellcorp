/* tslint:disable:no-unused-variable */

import { TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';


describe('Service: Auth', () => {
  // We declare the variables that we'll use for the Test Controller and for our Service
  let httpTestingController: HttpTestingController;
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(AuthService);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', inject([AuthService], () => {
    expect(service).toBeTruthy();
  }));

  it('returned Observable should match the right data', () => {
    // tslint:disable-next-line:prefer-const
    let model: any = {};
    model.username = 'Admin';
    model.password = 'password';
    const baseUrl = environment.apiUrl + 'login';

    service.login(model)
      .subscribe((data: any) => {
        expect(data.token_type).toEqual('bearer');
      });

    const req = httpTestingController.expectOne(baseUrl);

    expect(req.request.method).toEqual('POST');
  });
});
