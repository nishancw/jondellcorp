import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { tokenName } from '@angular/compiler';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.apiUrl;
  userToken: any;
  isAdmin: boolean;
  userName: any;
  private adminString = 'CanUploadAndViewReport';

constructor(private http: HttpClient) { }
  login(model: any) {
    const headers = new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded'});
    const body = new HttpParams()
    .set('username', model.username)
    .set('password', model.password)
    .set('grant_type', 'password');
    const options = { headers };
    return this.http.post<any>(this.baseUrl + 'login', body, options).pipe(map(response => {
      const user = response;
      if (user) {
        localStorage.setItem('token', user.access_token);
        localStorage.setItem('user', user.userName);
        localStorage.setItem('isAdmin', user.roles.includes(this.adminString));
        this.userToken = user.access_token;
        this.isAdmin = user.roles.includes(this.adminString);
        this.userName = user.userName;
      }
    }))
    .pipe(
      catchError(this.handleError)
    );
  }

  register(model: any) {
    // console.log(model);
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    const options = { headers };
    return this.http.post<any>(this.baseUrl + 'api/account/register', model, options)
    .pipe(
      catchError(this.handleError)
    );
  }

  loggedIn() {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

  loggedInAsAdmin() {
    if (localStorage.getItem('isAdmin') === 'true') {
      return true;
    } else {
      return false;
    }
  }

  private handleError(error: any) {
    const applicationError = error.error.error_description;
    if (applicationError) {
      return throwError(applicationError);
    }
    const serverError = error.error.modelState;
    let modelStateErrors = '';
    if (serverError) {
      for (const key in serverError) {
        if (serverError[key]) {
          modelStateErrors += serverError[key] + '\n';
        }
      }
    }
    return throwError(modelStateErrors || 'Server error');
  }
}
