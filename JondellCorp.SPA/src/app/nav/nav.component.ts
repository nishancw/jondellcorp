import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  model: any = {};
  isCollapsed = false;
  constructor(public authService: AuthService, private alertify: AlertifyService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.model).subscribe(data => {
      this.alertify.success('logged in');
    }, error => {
      this.alertify.error(error);
    }, () => {
      this.router.navigate(['/home']);
    });
  }

  logout() {
    this.authService.userToken = null;
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.removeItem('isAdmin');
    this.router.navigate(['/home']);
    this.alertify.message('logged out');
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    return !!token;
  }

  isAdmin() {
    const admin = localStorage.getItem('isAdmin');
    if (admin === 'true') {
      return true;
    } else {
      return false;
    }
  }
}
