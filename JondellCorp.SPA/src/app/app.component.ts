import { Component, OnInit, HostListener } from '@angular/core';
import { AuthService } from './_services/auth.service';
import { Spinkit } from 'ng-http-loader';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Jondell Corp';
  spinnerStyle = Spinkit;

  constructor(private authService: AuthService, private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle( this.title );
    const token = localStorage.getItem('token');
    if (token) {
      this.authService.userName = localStorage.getItem('user');
    }
  }

  @HostListener('window:onbeforeunload', ['$event'])
  beforeunloadHandler(event) {
    localStorage.removeItem('token');
  }
}
