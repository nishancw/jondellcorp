import { Component, OnInit } from '@angular/core';
import { Balance } from '../_models/Balance';
import { BalanceService } from '../_services/balance.service';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css']
})
export class BalanceComponent implements OnInit {
  balance: Balance;
  months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];
  monthList = [
    {value: 1, display: 'January'},
    {value: 2, display: 'February'},
    {value: 3, display: 'March'},
    {value: 4, display: 'April'},
    {value: 5, display: 'May'},
    {value: 6, display: 'June'},
    {value: 7, display: 'July'},
    {value: 8, display: 'August'},
    {value: 9, display: 'September'},
    {value: 10, display: 'October'},
    {value: 11, display: 'November'},
    {value: 12, display: 'December'}];
  yearList: any = [];
  balanceParams: any = {};
  constructor(private balanceService: BalanceService, private alertify: AlertifyService) { }

  ngOnInit() {
    this.loadBalance();
  }

  loadBalance() {
    this.balanceService.getLastBalance().subscribe((balance: Balance) => {
      // console.log(balance);
      this.balance = balance;
      this.balanceParams.year = this.balance.year;
      this.balanceParams.month = this.balance.month;
      this.balance.month = this.months[balance.month as any - 1];
    }, error => {
      this.alertify.error(error);
    });

    this.balanceService.getYearList().subscribe((responce) => {
      this.yearList = responce;
    }, error => {
      this.alertify.error(error);
    });
  }

  getBalanceByUserParams() {
    this.balanceService.getBalance(this.balanceParams).subscribe((balance: Balance) => {
      // console.log('getBalance', balance);
      if (balance) {
      this.balance = balance;
      this.balanceParams.year = this.balance.year;
      this.balanceParams.month = this.balance.month;
      this.balance.month = this.months[balance.month as any - 1];
      }
    }, error => {
      this.alertify.error(error);
    });
  }
}
