import { Component, OnInit } from '@angular/core';
import { BalanceService } from '../_services/balance.service';
import { AlertifyService } from '../_services/alertify.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label, MultiDataSet } from 'ng2-charts';
import { Report } from '../_models/Report';
import { Balance } from '../_models/Balance';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  monthList = [
    {value: 1, display: 'January'},
    {value: 2, display: 'February'},
    {value: 3, display: 'March'},
    {value: 4, display: 'April'},
    {value: 5, display: 'May'},
    {value: 6, display: 'June'},
    {value: 7, display: 'July'},
    {value: 8, display: 'August'},
    {value: 9, display: 'September'},
    {value: 10, display: 'October'},
    {value: 11, display: 'November'},
    {value: 12, display: 'December'}];
  accountList = [
    {value: 'randd', display: 'R&D'},
    {value: 'canteen', display: 'Canteen'},
    {value: 'ceoscar', display: 'CEO’s Car'},
    {value: 'marketing', display: 'Marketing'},
    {value: 'parkingFines', display: 'Parking Fines'}];
  yearList: any = [];
  userParams: any = {};
  balanceParams: any = {};
  accBalances: number[] = [];
  balance: Balance = {
    year: 0,
    month: '',
    randD: 0,
    canteen: 0,
    ceOsCar: 0,
    marketing: 0,
    parkingFines: 0,
  };
  accBalancesForAcc: number[] = [];

  doughnutChartLabels: Label[] = ['R&D', 'Canteen', 'CEO’s Car', 'Marketing', 'Parking Fines'];
  doughnutChartData: MultiDataSet = [
    this.accBalancesForAcc
  ];
  doughnutChartType: ChartType = 'doughnut';


  lineChartData: ChartDataSets[] = [
    { data: this.accBalances, label: 'Account Balance' },
  ];

  lineChartLabels: Label[] = [];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'rgb(0, 255, 255)',
      backgroundColor: 'rgb(0, 51, 51)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  constructor(private balanceService: BalanceService, private alertify: AlertifyService) { }

  ngOnInit() {
    this.setStage();
  }

  resetFilters() {
    this.setStage();
  }

  setStage() {
    this.userParams.startYear = new Date().getFullYear();
    this.userParams.startMonth = 1;
    this.userParams.endYear = new Date().getFullYear();
    this.userParams.endMonth = new Date().getMonth() + 1;
    this.userParams.account = 'randd';
    this.balanceParams.month = 1;
    this.balanceParams.year = new Date().getFullYear();
    if (this.userParams.startMonth === this.userParams.endMonth) {
      this.userParams.startYear -= 1;
    }
    this.loadData();
  }

  loadData() {
    this.accBalances.length = 0;
    this.accBalancesForAcc.length = 0;
    this.lineChartLabels.length = 0;
    // console.log(this.userParams);
    this.balanceService.getReport(this.userParams).subscribe((responce) => {
      // console.log(responce);
      for (const result of responce) {
        this.accBalances.push(result.accBalance);
        this.lineChartLabels.push(result.year + ' / ' + result.month);
      }
    }, error => {
      this.alertify.error(error);
    });

    this.balanceService.getYearList().subscribe((responce) => {
      this.yearList = responce;
    }, error => {
      this.alertify.error(error);
    });

    this.balanceService.getLastBalance().subscribe((balance: Balance) => {
      // console.log('getLastBalance', balance);
      if (balance) {
        this.accBalancesForAcc.push(balance.randD, balance.canteen, balance.ceOsCar, balance.marketing, balance.parkingFines);
        this.balanceParams.month = balance.month;
      }
    }, error => {
      this.alertify.error(error);
    });
  }

  loadBalance() {
    this.accBalancesForAcc.length = 0;
    this.balanceService.getBalance(this.balanceParams).subscribe((balance: Balance) => {
      // console.log('getBalance', balance);
      if (balance) {
        this.accBalancesForAcc.push(balance.randD, balance.canteen, balance.ceOsCar, balance.marketing, balance.parkingFines);
      }
    }, error => {
      this.alertify.error(error);
    });
  }

  downloadCanvas1(event1) {
    const anchor = event1.target;
    anchor.href = document.getElementsByTagName('canvas')[0].toDataURL();
    anchor.download = 'chart' + 1 + '.png';
  }

  downloadCanvas2(event2) {
    const anchor = event2.target;
    anchor.href = document.getElementsByTagName('canvas')[1].toDataURL();
    anchor.download = 'chart' + 2 + '.png';
  }
}
