import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: any = {};
  roles = [
    {id: 'CanUploadAndViewReport', name: 'Admin priviledges'},
    {id: 'CanViewBalance', name: 'User priviledges'}
  ];
  @Output() cancelRegister = new EventEmitter();
  constructor(private authService: AuthService, private alertify: AlertifyService, private router: Router) { }

  ngOnInit() {
  }

  register() {
    if (this.model.password === this.model.confirmPassword) {
      this.authService.register(this.model).subscribe(() => {
        this.alertify.success('registration successful');
        this.model = {};
        this.cancel();
      }, error => {
        this.alertify.error(error);
      });
    } else {
      this.alertify.error('Password mismatch');
    }
  }

  cancel() {
    this.cancelRegister.emit(false);
    // console.log('cancelled');
  }

}
