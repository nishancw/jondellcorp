import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { NavComponent } from './nav/nav.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './_services/auth.service';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { AlertifyService } from './_services/alertify.service';
import { BsDropdownModule, CollapseModule } from 'ngx-bootstrap';
import { UploadComponent } from './upload/upload.component';
import { BalanceComponent } from './balance/balance.component';
import { ReportComponent } from './report/report.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { AuthGuard } from './_guards/auth.guard';
import { AdminAuthGuard } from './_guards/admin-auth.guard';
import { UserAuthGuard } from './_guards/user-auth.guard';
import { BalanceService } from './_services/balance.service';
import { ChartsModule } from 'ng2-charts';
import { NgHttpLoaderModule } from 'ng-http-loader';

@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      HomeComponent,
      RegisterComponent,
      UploadComponent,
      BalanceComponent,
      ReportComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule,
      CollapseModule.forRoot(),
      BsDropdownModule.forRoot(),
      RouterModule.forRoot(appRoutes),
      ChartsModule,
      NgHttpLoaderModule.forRoot()
   ],
   providers: [
      AuthService,
      AlertifyService,
      AuthGuard,
      AdminAuthGuard,
      UserAuthGuard,
      BalanceService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
