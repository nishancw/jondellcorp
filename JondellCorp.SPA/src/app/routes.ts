import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UploadComponent } from './upload/upload.component';
import { BalanceComponent } from './balance/balance.component';
import { ReportComponent } from './report/report.component';
import { AuthGuard } from './_guards/auth.guard';
import { AdminAuthGuard } from './_guards/admin-auth.guard';
import { UserAuthGuard } from './_guards/user-auth.guard';

export const appRoutes: Routes = [
{path: 'home', component: HomeComponent},
{path: 'upload', component: UploadComponent, canActivate: [AuthGuard, AdminAuthGuard]},
{path: 'balance', component: BalanceComponent, canActivate: [AuthGuard, UserAuthGuard]},
{path: 'report', component: ReportComponent, canActivate: [AuthGuard, AdminAuthGuard]},
{path: '**', redirectTo: 'home', pathMatch: 'full'},
];
