export interface Report {
    year: number;
    month: number;
    accBalance: number;
}
