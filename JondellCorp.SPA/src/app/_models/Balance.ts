export interface Balance {
    year: number;
    month: string;
    randD: number;
    canteen: number;
    ceOsCar: number;
    marketing: number;
    parkingFines: number;
}
