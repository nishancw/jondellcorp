export const environment = {
  production: true,
  apiUrl: 'https://api-jondellcorp.azurewebsites.net/',
  debugMode: false
};
